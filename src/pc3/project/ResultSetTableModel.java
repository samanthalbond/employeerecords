package pc3.project;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.*;
public class ResultSetTableModel extends AbstractTableModel {

    private ArrayList data;
    private ResultSet resultSet;
    private ResultSetMetaData meta;
    
    //constructor to use resultSet to set the model for JTable
    public ResultSetTableModel(ResultSet rs)throws SQLException{
        resultSet = rs;
        meta = resultSet.getMetaData();
        data = new ArrayList();
        int cols = meta.getColumnCount();

        while (resultSet.next()){
            Object[] row = new Object[cols];
            for (int i = 0; i < row.length; i++){
                row[i] = resultSet.getObject(i + 1);
            }
            data.add(row);
        }
    }

    @Override
    public int getRowCount(){
        return data.size();
    }

    @Override
    public int getColumnCount(){
        try {
            return meta.getColumnCount();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            return 0;
        }
    }

    @Override
    public Object getValueAt(int row, int col) {
        if (row < data.size()){
            return ((Object[])data.get(row))[col];
        }
        else{
            return null;
        }
    }
    
    @Override
    public String getColumnName(int count){
        try{
        return meta.getColumnName(++count);
        }
        catch (SQLException sqle){
            
            System.out.println(sqle.toString());
            return "";
        }
    }

   

    
}
