package pc3.project;
import java.sql.*;
public class DatabaseConnector {

    //method to allow program to connect to database
    public static Connection connectToDatabase() throws SQLException{
        String url = "jdbc:mysql://localhost:3306/";
        String database = "employeedatabase";
        String username = "root";
        String password = "password";
        return DriverManager.getConnection(url + database, username, password);
    }
    //method to close database connection
    public static void closeConnection(Connection con) throws SQLException{
        con.close();
    }
}
