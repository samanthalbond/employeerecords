package pc3.project;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.*;
import javax.swing.JOptionPane;

public class EmployeeFrame extends javax.swing.JFrame {

    
    public EmployeeFrame() {
        initComponents();
        setStartingRecord();
        setMetaData();
    }

    //declaring statement and connection to be instantiated throughout code where needed
    Connection con = null;
    Statement stmt = null;
    
    //declaring variable to store query results and save to file
    public static String results;

   
    //method to set the initial record to show when program starts, based on
    //the lowest ID number
    public void setStartingRecord(){
        try{
            con = DatabaseConnector.connectToDatabase();
            stmt = con.createStatement();
            String sql = "SELECT * FROM empdetail WHERE EmpID=(SELECT MIN(EmpID) FROM empdetail)";
            ResultSet resultSet = stmt.executeQuery(sql);
            while (resultSet.next()){
                txtEmpNo.setText(resultSet.getString("EmpID"));
                txtName.setText(resultSet.getString("EmpName"));
                txtAddress.setText(resultSet.getString("Address"));
                txtSuburb.setText(resultSet.getString("Suburb"));
                txtPostcode.setText(resultSet.getString("Postcode"));
                txtPhone.setText(resultSet.getString("HomePhone"));
                txtMobile.setText(resultSet.getString("Mobile"));
                txtEmail.setText(resultSet.getString("Email"));
            }
            DatabaseConnector.closeConnection(con);
            stmt.close();
        }
        catch (SQLException sqle){
            msgMessage.showMessageDialog(this, sqle.toString(), "Error Message", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    //method to clear textboxes
    public void clearTextboxes(){
        txtName.setText("");
        txtAddress.setText("");
        txtSuburb.setText("");
        txtPostcode.setText("");
        txtPhone.setText("");
        txtMobile.setText("");
        txtEmail.setText("");
    }
    
    //method to retrieve metadata for the database, and set textboxes in 
    //metadata panel when program starts
    public void setMetaData(){
        try{
            con = DatabaseConnector.connectToDatabase();
            stmt = con.createStatement();
            String sql = "SELECT * FROM empdetail";
            ResultSet resultSet = stmt.executeQuery(sql);
            ResultSetMetaData meta = resultSet.getMetaData();
            DatabaseMetaData md = con.getMetaData();

            int cols = meta.getColumnCount();
            
            txtDatabaseType.setText(md.getDatabaseProductName());
            txtDatabaseVersion.setText(md.getDatabaseProductVersion());
            txtDatabaseDriver.setText(md.getDriverName());
            txtTableName.setText(meta.getTableName(1));
            for (int i = 1; i <= cols; i++){
                txtTableColumns.setText(txtTableColumns.getText() + "\n" + meta.getColumnName(i) + "\t" + meta.getColumnTypeName(i));
            }
            stmt.close();
            DatabaseConnector.closeConnection(con);
        }
        catch (SQLException sqle){
            msgMessage.showMessageDialog(this, sqle.toString(), "Error Message",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    //method to provide functionality for the submit query button
    //also uses instance of ResultSetTableModel to set results arraylist
    //so data can be saved to textfile
    public String submitButton(){
        String sql = txtQuery.getText();
        ResultSet resultSet;
        try{
            con = DatabaseConnector.connectToDatabase();
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            resultSet = stmt.executeQuery(sql);
            ResultSetTableModel model = new ResultSetTableModel(resultSet);
            tblResults.setModel(model);
            
            for (int col = 0; col < model.getColumnCount(); col++){
                results += model.getColumnName(col) + " | ";
            }
            if(!resultSet.next()){
                resultSet.beforeFirst();
                while(resultSet.next()){
                    results += "\r\n" + resultSet.getString("EmpID") + " | ";
                    results += resultSet.getString("EmpName") + " | ";
                    results += resultSet.getString("Address") + " | ";
                    results += resultSet.getString("Suburb") + " | ";
                    results += resultSet.getString("Postcode") + " | ";
                    results += resultSet.getString("HomePhone") + " | ";
                    results += resultSet.getString("Mobile") + " | ";
                    results += resultSet.getString("Email");
                }
            }
            txtNumberOfRecords.setText(String.valueOf(model.getRowCount()));
            DatabaseConnector.closeConnection(con);
            stmt.close();
            return String.valueOf(results);
        }
        catch (SQLException sqle){
            msgMessage.showMessageDialog(this, sqle.toString(), "Error Message", JOptionPane.ERROR_MESSAGE);
            return "";
        }
    }

  
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        msgMessage = new javax.swing.JOptionPane();
        panEmployee = new javax.swing.JPanel();
        lblEmpNo = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        lblAddress = new javax.swing.JLabel();
        lblSuburb = new javax.swing.JLabel();
        lblPostcode = new javax.swing.JLabel();
        lblPhone = new javax.swing.JLabel();
        lblMobile = new javax.swing.JLabel();
        txtEmpNo = new javax.swing.JTextField();
        txtName = new javax.swing.JTextField();
        txtAddress = new javax.swing.JTextField();
        txtSuburb = new javax.swing.JTextField();
        txtPostcode = new javax.swing.JTextField();
        txtPhone = new javax.swing.JTextField();
        txtMobile = new javax.swing.JTextField();
        lblEmail = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        btnNew = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnRetrieve = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        panMetaData = new javax.swing.JPanel();
        lblDatabaseType = new javax.swing.JLabel();
        lblDatabaseVersion = new javax.swing.JLabel();
        lblDatabaseDriver = new javax.swing.JLabel();
        lblTableName = new javax.swing.JLabel();
        lblTableColumns = new javax.swing.JLabel();
        txtDatabaseType = new javax.swing.JTextField();
        txtDatabaseVersion = new javax.swing.JTextField();
        txtDatabaseDriver = new javax.swing.JTextField();
        txtTableName = new javax.swing.JTextField();
        srlMetaData = new javax.swing.JScrollPane();
        txtTableColumns = new javax.swing.JTextArea();
        panQuery = new javax.swing.JPanel();
        lblQuery = new javax.swing.JLabel();
        lblResults = new javax.swing.JLabel();
        lblNumberOfRecords = new javax.swing.JLabel();
        lblReportFile = new javax.swing.JLabel();
        txtQuery = new javax.swing.JTextField();
        btnSubmitQuery = new javax.swing.JButton();
        txtNumberOfRecords = new javax.swing.JTextField();
        txtReportFile = new javax.swing.JTextField();
        btnReportFile = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblResults = new javax.swing.JTable();
        lblHeading = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Employee Details Form");

        panEmployee.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Employee Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N

        lblEmpNo.setText("Employee No:");

        lblName.setText("Name:");

        lblAddress.setText("Address:");

        lblSuburb.setText("Suburb:");

        lblPostcode.setText("Postcode:");

        lblPhone.setText("Phone (Home):");

        lblMobile.setText("Mobile:");

        lblEmail.setText("Email:");

        btnNew.setText("New");
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnRetrieve.setText("Retrieve");
        btnRetrieve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRetrieveActionPerformed(evt);
            }
        });

        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panEmployeeLayout = new javax.swing.GroupLayout(panEmployee);
        panEmployee.setLayout(panEmployeeLayout);
        panEmployeeLayout.setHorizontalGroup(
            panEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panEmployeeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panEmployeeLayout.createSequentialGroup()
                        .addGroup(panEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblEmpNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblPostcode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblSuburb, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblAddress, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblPhone, javax.swing.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
                            .addComponent(lblEmail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblMobile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(52, 52, 52)
                        .addGroup(panEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtName)
                            .addComponent(txtAddress)
                            .addComponent(txtSuburb)
                            .addComponent(txtPostcode)
                            .addComponent(txtPhone)
                            .addComponent(txtMobile)
                            .addComponent(txtEmpNo)
                            .addComponent(txtEmail))
                        .addContainerGap())
                    .addGroup(panEmployeeLayout.createSequentialGroup()
                        .addComponent(btnNew, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRetrieve, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 9, Short.MAX_VALUE))))
        );
        panEmployeeLayout.setVerticalGroup(
            panEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panEmployeeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEmpNo)
                    .addComponent(txtEmpNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAddress)
                    .addComponent(txtAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSuburb)
                    .addComponent(txtSuburb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPostcode)
                    .addComponent(txtPostcode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPhone)
                    .addComponent(txtPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMobile)
                    .addComponent(txtMobile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEmail)
                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(panEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnNew)
                    .addComponent(btnUpdate)
                    .addComponent(btnDelete)
                    .addComponent(btnRetrieve))
                .addGap(0, 8, Short.MAX_VALUE))
        );

        panMetaData.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Meta Data", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N

        lblDatabaseType.setText("Database Type:");

        lblDatabaseVersion.setText("Database Version:");

        lblDatabaseDriver.setText("Database Driver:");

        lblTableName.setText("Table Name:");

        lblTableColumns.setText("Table Columns:");

        txtDatabaseType.setEditable(false);

        txtDatabaseVersion.setEditable(false);

        txtDatabaseDriver.setEditable(false);

        txtTableName.setEditable(false);

        txtTableColumns.setEditable(false);
        txtTableColumns.setColumns(20);
        txtTableColumns.setRows(5);
        srlMetaData.setViewportView(txtTableColumns);

        javax.swing.GroupLayout panMetaDataLayout = new javax.swing.GroupLayout(panMetaData);
        panMetaData.setLayout(panMetaDataLayout);
        panMetaDataLayout.setHorizontalGroup(
            panMetaDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panMetaDataLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panMetaDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblDatabaseType, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblDatabaseVersion, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                    .addComponent(lblDatabaseDriver, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblTableName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblTableColumns, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(panMetaDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDatabaseType)
                    .addComponent(txtDatabaseVersion)
                    .addComponent(txtDatabaseDriver)
                    .addComponent(txtTableName)
                    .addComponent(srlMetaData))
                .addContainerGap())
        );
        panMetaDataLayout.setVerticalGroup(
            panMetaDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panMetaDataLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panMetaDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panMetaDataLayout.createSequentialGroup()
                        .addGroup(panMetaDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblDatabaseType)
                            .addComponent(txtDatabaseType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panMetaDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblDatabaseVersion)
                            .addComponent(txtDatabaseVersion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panMetaDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblDatabaseDriver)
                            .addComponent(txtDatabaseDriver, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(lblTableName))
                    .addComponent(txtTableName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panMetaDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTableColumns)
                    .addComponent(srlMetaData, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panQuery.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Query", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        panQuery.setPreferredSize(new java.awt.Dimension(399, 340));

        lblQuery.setText("Query:");

        lblResults.setText("Results:");

        lblNumberOfRecords.setText("Number of Records:");

        lblReportFile.setText("Report File:");

        btnSubmitQuery.setText("Submit Query");
        btnSubmitQuery.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitQueryActionPerformed(evt);
            }
        });

        txtNumberOfRecords.setEditable(false);

        btnReportFile.setText("Report File");
        btnReportFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReportFileActionPerformed(evt);
            }
        });

        tblResults.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblResults);

        javax.swing.GroupLayout panQueryLayout = new javax.swing.GroupLayout(panQuery);
        panQuery.setLayout(panQueryLayout);
        panQueryLayout.setHorizontalGroup(
            panQueryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panQueryLayout.createSequentialGroup()
                .addGroup(panQueryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panQueryLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panQueryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panQueryLayout.createSequentialGroup()
                                .addGroup(panQueryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(lblResults, javax.swing.GroupLayout.DEFAULT_SIZE, 59, Short.MAX_VALUE)
                                    .addComponent(lblQuery, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtQuery, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panQueryLayout.createSequentialGroup()
                                .addComponent(lblReportFile, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtReportFile, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnReportFile, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panQueryLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btnSubmitQuery, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panQueryLayout.createSequentialGroup()
                        .addGap(123, 123, 123)
                        .addComponent(lblNumberOfRecords, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNumberOfRecords))
                    .addGroup(panQueryLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panQueryLayout.setVerticalGroup(
            panQueryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panQueryLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panQueryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuery)
                    .addComponent(txtQuery, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSubmitQuery)
                .addGap(25, 25, 25)
                .addComponent(lblResults)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panQueryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNumberOfRecords)
                    .addComponent(txtNumberOfRecords, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(148, 148, 148)
                .addGroup(panQueryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblReportFile)
                    .addComponent(txtReportFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnReportFile))
                .addGap(20, 20, 20))
        );

        lblHeading.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblHeading.setText("Employee Details");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblHeading, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(panEmployee, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panMetaData, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(panQuery, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(37, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblHeading)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(panMetaData, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(panQuery, javax.swing.GroupLayout.DEFAULT_SIZE, 611, Short.MAX_VALUE))
                .addGap(21, 21, 21))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        clearTextboxes();
        txtEmpNo.setText("");
        try{
            con = DatabaseConnector.connectToDatabase();
            stmt = con.createStatement();
            String sql = "SELECT MAX(EmpID) FROM empdetail";
            ResultSet resultSet = stmt.executeQuery(sql);
            int newID = 0;
            while(resultSet.next()){
                newID = resultSet.getInt(1);
            }
            txtEmpNo.setText(String.valueOf(newID + 1));
            DatabaseConnector.closeConnection(con);
            stmt.close();
        }
        catch (SQLException sqle){
            msgMessage.showMessageDialog(this, sqle.toString(), "Error Message",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnNewActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        try{
            con = DatabaseConnector.connectToDatabase();
            stmt = con.createStatement();
            String sql = "INSERT INTO empdetail VALUES(";
            sql += txtEmpNo.getText() + ", '";
            sql += txtName.getText() + "', '";
            sql += txtAddress.getText() + "', '";
            sql += txtSuburb.getText() + "', '";
            sql += txtPostcode.getText() + "', '";
            sql += txtPhone.getText() + "', '";
            sql += txtMobile.getText() + "', '";
            sql += txtEmail.getText() + "')";
            
            stmt.executeUpdate(sql);
            msgMessage.showMessageDialog(this, "Record added to current table "
                    + "successfully", "Message", JOptionPane.INFORMATION_MESSAGE);
            stmt.close();
            DatabaseConnector.closeConnection(con);
        }
        catch (SQLException sqle){
            msgMessage.showMessageDialog(this, sqle.toString(), "Error Message", 
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnRetrieveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRetrieveActionPerformed
        try{
            if (txtEmpNo.getText().length() == 0){
                msgMessage.showMessageDialog(this, "Please enter a EmpID to "
                        + "search for records", "Information Message", 
                        JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            con = DatabaseConnector.connectToDatabase();
            stmt = con.createStatement();
            String sql = "SELECT * FROM empdetail WHERE EmpID = " + txtEmpNo.getText();
            ResultSet resultSet = stmt.executeQuery(sql);
            if (!resultSet.next()){
                clearTextboxes();
                msgMessage.showMessageDialog(this, "No record exists with this "
                        + "employee ID", "Information Message", 
                        JOptionPane.INFORMATION_MESSAGE);
            }
            else{
                do {
                txtEmpNo.setText(resultSet.getString("EmpID"));
                txtName.setText(resultSet.getString("EmpName"));
                txtAddress.setText(resultSet.getString("Address"));
                txtSuburb.setText(resultSet.getString("Suburb"));
                txtPostcode.setText(resultSet.getString("Postcode"));
                txtPhone.setText(resultSet.getString("HomePhone"));
                txtMobile.setText(resultSet.getString("Mobile"));
                txtEmail.setText(resultSet.getString("Email"));  
                } while (resultSet.next()); 
            }
            stmt.close();
            DatabaseConnector.closeConnection(con);
        }
        catch (SQLException sqle){
            msgMessage.showMessageDialog(this, sqle.toString(),
                    "Error Message", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnRetrieveActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        try{
            if (txtEmpNo.getText().length() == 0){
                    msgMessage.showMessageDialog(this, "Please enter a EmpID to "
                            + "search for records", "Information Message", 
                            JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
            con = DatabaseConnector.connectToDatabase();
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            String sql = "SELECT * FROM empdetail WHERE EmpId = " + txtEmpNo.getText();
            ResultSet resultSet = stmt.executeQuery(sql);
            resultSet.absolute(1);
            resultSet.updateString("EmpId", txtEmpNo.getText());
            resultSet.updateString("EmpName", txtName.getText());
            resultSet.updateString("Address", txtAddress.getText());
            resultSet.updateString("Suburb", txtSuburb.getText());
            resultSet.updateString("Postcode", txtPostcode.getText());
            resultSet.updateString("HomePhone", txtPhone.getText());
            resultSet.updateString("Mobile", txtMobile.getText());
            resultSet.updateString("Email", txtEmail.getText());
            resultSet.updateRow();
            
            msgMessage.showMessageDialog(this, "Record has been updated successfully", 
                    "Information Message", JOptionPane.INFORMATION_MESSAGE);
            stmt.close();
            DatabaseConnector.closeConnection(con);
        }
        catch (SQLException sqle){
            msgMessage.showMessageDialog(this, sqle.toString(),
                    "Error Message", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        try{
            con = DatabaseConnector.connectToDatabase();
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            String sql = "SELECT * FROM empdetail WHERE EmpId = " + txtEmpNo.getText();
            ResultSet resultSet = stmt.executeQuery(sql);
            if (resultSet.next()){
                resultSet.deleteRow();
                msgMessage.showMessageDialog(this, "Record has been deleted "
                        + "successfully", "Information Message", JOptionPane.INFORMATION_MESSAGE);
                clearTextboxes();
                txtEmpNo.setText("");
            }
            stmt.close();
            DatabaseConnector.closeConnection(con);
        }
        catch(SQLException sqle){
            msgMessage.showMessageDialog(this, sqle.toString(), "Error Message",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnSubmitQueryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitQueryActionPerformed
       submitButton();
    }//GEN-LAST:event_btnSubmitQueryActionPerformed

    private void btnReportFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReportFileActionPerformed
        System.out.println(results);
        Writer writer = null;
        try{
            writer = new FileWriter(txtReportFile.getText());
            writer.write(results.toString().replace(",", ""));
        }
        catch (Exception e){
            msgMessage.showMessageDialog(this, e.toString(), "Error Message", JOptionPane.ERROR_MESSAGE);
        }
        finally{
            if (writer != null){
                try{
                    writer.close();
                }
                catch (IOException ioe){
                    msgMessage.showMessageDialog(this, ioe.toString(), "Error Message", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }//GEN-LAST:event_btnReportFileActionPerformed

    
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EmployeeFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnReportFile;
    private javax.swing.JButton btnRetrieve;
    private javax.swing.JButton btnSubmitQuery;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblAddress;
    private javax.swing.JLabel lblDatabaseDriver;
    private javax.swing.JLabel lblDatabaseType;
    private javax.swing.JLabel lblDatabaseVersion;
    private javax.swing.JLabel lblEmail;
    private javax.swing.JLabel lblEmpNo;
    private javax.swing.JLabel lblHeading;
    private javax.swing.JLabel lblMobile;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblNumberOfRecords;
    private javax.swing.JLabel lblPhone;
    private javax.swing.JLabel lblPostcode;
    private javax.swing.JLabel lblQuery;
    private javax.swing.JLabel lblReportFile;
    private javax.swing.JLabel lblResults;
    private javax.swing.JLabel lblSuburb;
    private javax.swing.JLabel lblTableColumns;
    private javax.swing.JLabel lblTableName;
    private javax.swing.JOptionPane msgMessage;
    private javax.swing.JPanel panEmployee;
    private javax.swing.JPanel panMetaData;
    private javax.swing.JPanel panQuery;
    private javax.swing.JScrollPane srlMetaData;
    private javax.swing.JTable tblResults;
    private javax.swing.JTextField txtAddress;
    private javax.swing.JTextField txtDatabaseDriver;
    private javax.swing.JTextField txtDatabaseType;
    private javax.swing.JTextField txtDatabaseVersion;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtEmpNo;
    private javax.swing.JTextField txtMobile;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtNumberOfRecords;
    private javax.swing.JTextField txtPhone;
    private javax.swing.JTextField txtPostcode;
    private javax.swing.JTextField txtQuery;
    private javax.swing.JTextField txtReportFile;
    private javax.swing.JTextField txtSuburb;
    private javax.swing.JTextArea txtTableColumns;
    private javax.swing.JTextField txtTableName;
    // End of variables declaration//GEN-END:variables

}
